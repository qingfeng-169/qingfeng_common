package slice

import (
	"errors"
)

// Copy 复制操作
func Copy[T any](s []T) []T {
	out := make([]T, len(s))
	copy(out, s)
	return out
}

// Append使用go的内置方法即可。如
// a = append(a,b...)

// Delete 删除下标为i的元素
func Delete[T any](s []T, i int) ([]T, error) {
	if i < 0 || i >= len(s) {
		return nil, errors.New("index out of range")
	}

	copy(s[i:], s[i+1:])

	var zero T
	s[len(s)-1] = zero
	s = s[:len(s)-1]
	return s, nil
}

func Insert[T any](s []T, i int, v T) ([]T, error) {
	if i < 0 || i > len(s) {
		return nil, errors.New("index out of range")
	}
	s = append(s[:i], append([]T{v}, s[i:]...)...)
	return s, nil
}

// Filter 函数接受一个切片和一个函数作为参数
// keep 函数应当返回一个布尔值，表示是否保留该元素
// 返回一个新的切片，包含所有满足条件的元素
func Filter[T any](s []T, keep func(T) bool) []T {
	var filtered []T
	for _, item := range s {
		if keep(item) {
			filtered = append(filtered, item)
		}
	}
	return filtered
}

// Push 在末尾追加元素，不考虑内存拷贝的情况，复杂度为 O(1)。
func Push[T any](s []T, item T) []T {
	s = append(s, item)
	return s
}

// Unshift (push front/unshift)在头部追加元素，时间和空间复杂度均为 O(N)，不推荐。
func Unshift[T any](s []T, item T) []T {
	s = append([]T{item}, s...)
	return s
}

// Pop 尾部删除元素
func Pop[T any](s []T) (T, []T, error) {
	var zero T
	if len(s) == 0 {
		err := errors.New("empty slice")
		return zero, nil, err
	}
	//a := s
	e := s[len(s)-1]
	s[len(s)-1] = zero
	s = s[:len(s)-1]
	//fmt.Println(a)
	return e, s, nil
}

// Shift (pop front)头部删除元素
func Shift[T any](s []T) (T, []T, error) {
	var zero T
	if len(s) == 0 {
		err := errors.New("empty slice")
		return zero, nil, err
	}
	//a := s
	e := s[0]
	s[0] = zero
	s = s[1:]
	//fmt.Println(a)
	return e, s, nil
}
