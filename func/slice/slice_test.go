package slice

import (
	"fmt"
	"reflect"
	"testing"
)

func TestDelete(t *testing.T) {
	type args[T any] struct {
		s []T
		i int
	}
	type testCase[T any] struct {
		name    string
		args    args[T]
		want    []T
		wantErr bool
	}
	tests := []testCase[int]{
		{
			name: "test1",
			args: args[int]{
				s: []int{0, 1, 2, 3, 4, 5},
				i: 3,
			},
			want:    []int{0, 1, 2, 4, 5},
			wantErr: false,
		},
		{
			name: "test2",
			args: args[int]{
				s: []int{0, 1, 2, 3, 4, 5},
				i: 5,
			},
			want:    []int{0, 1, 2, 3, 4},
			wantErr: false,
		},
		{
			name: "test3",
			args: args[int]{
				s: []int{0, 1, 2, 3, 4, 5},
				i: 6,
			},
			want:    nil,
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := Delete(tt.args.s, tt.args.i)
			if (err != nil) != tt.wantErr {
				t.Errorf("Delete() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Delete() got = %v, want %v", got, tt.want)
			}
		})
	}
}

func ExampleCopy() {
	a := []int{0, 1, 2, 3, 4, 5}
	cp := Copy(a)
	fmt.Println(cp)
	// Output:
	// [0 1 2 3 4 5]
}

func ExampleFilter() {
	// 示例：过滤一个整数切片，只保留偶数
	numbers := []int{0, 1, 2, 3, 4, 5}
	evenNumbers := Filter(numbers, func(n int) bool {
		return n%2 == 0
	})
	fmt.Println(evenNumbers)

	// 示例：过滤一个字符串切片，只保留长度大于3的字符串
	strings := []string{"a", "ab", "abc", "abcd", "abcde"}
	longStrings := Filter(strings, func(s string) bool {
		return len(s) > 3
	})
	fmt.Println(longStrings)
	// Output:
	// [0 2 4]
	// [abcd abcde]
}

func TestInsert(t *testing.T) {
	type args[T any] struct {
		s []T
		i int
		v T
	}
	type testCase[T any] struct {
		name    string
		args    args[T]
		want    []T
		wantErr bool
	}
	tests := []testCase[int]{
		// TODO: Add test cases.
		{
			name: "test1",
			args: args[int]{
				s: []int{0, 1, 2, 3, 4, 5},
				i: 3,
				v: 100,
			},
			want:    []int{0, 1, 2, 100, 3, 4, 5},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := Insert(tt.args.s, tt.args.i, tt.args.v)
			if (err != nil) != tt.wantErr {
				t.Errorf("Insert() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Insert() got = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestPop(t *testing.T) {
	type args[T any] struct {
		s []T
	}
	type testCase[T any] struct {
		name    string
		args    args[T]
		want    T
		want1   []T
		wantErr bool
	}
	tests := []testCase[int]{
		{
			name: "test1",
			args: args[int]{
				s: []int{0, 1, 2, 3, 4, 5},
			},
			want:    5,
			want1:   []int{0, 1, 2, 3, 4},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, got1, err := Pop(tt.args.s)
			if (err != nil) != tt.wantErr {
				t.Errorf("Pop() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Pop() got = %v, want %v", got, tt.want)
			}
			if !reflect.DeepEqual(got1, tt.want1) {
				t.Errorf("Pop() got1 = %v, want %v", got1, tt.want1)
			}
		})
	}
}

func TestShift(t *testing.T) {
	type args[T any] struct {
		s []T
	}
	type testCase[T any] struct {
		name    string
		args    args[T]
		want    T
		want1   []T
		wantErr bool
	}
	tests := []testCase[int]{
		{
			name: "test1",
			args: args[int]{
				s: []int{100, 1, 2, 3, 4, 5},
			},
			want:    100,
			want1:   []int{1, 2, 3, 4, 5},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, got1, err := Shift(tt.args.s)
			if (err != nil) != tt.wantErr {
				t.Errorf("Shift() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Shift() got = %v, want %v", got, tt.want)
			}
			if !reflect.DeepEqual(got1, tt.want1) {
				t.Errorf("Shift() got1 = %v, want %v", got1, tt.want1)
			}
		})
	}
}
