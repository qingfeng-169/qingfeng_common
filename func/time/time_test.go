package time

import "testing"

func TestChangeZoneTime(t *testing.T) {
    type args struct {
        fromTimeStr string
        fromZone    string
        toZone      string
        layout      string
    }
    tests := []struct {
        name          string
        args          args
        wantToTimeStr string
        wantErr       bool
    }{
        {
            name: "test1",
            args: args{
                fromTimeStr: "2000-10-10 10:00:00",
                fromZone:    "Asia/Shanghai",
                toZone:      "UTC",
                layout:      LayoutFormat(),
            },
            wantToTimeStr: "2000-10-10 02:00:00",
            wantErr:       false,
        },
        {
            name: "test2",
            args: args{
                fromTimeStr: "20001010 100000",
                fromZone:    "Asia/Shanghai",
                toZone:      "UTC",
                layout:      "20060102 150405",
            },
            wantToTimeStr: "20001010 020000",
            wantErr:       false,
        },
        {
            name: "test3",
            args: args{
                fromTimeStr: "2000-10-10 10:00:00",
                fromZone:    "Asia/Tokyo",
                toZone:      "Asia/Shanghai",
                layout:      LayoutFormat(),
            },
            wantToTimeStr: "2000-10-10 09:00:00",
            wantErr:       false,
        },
        {
            name: "test4",
            args: args{
                fromTimeStr: "2000-10-10 10:00:00",
                fromZone:    "Asia/Shanghai",
                toZone:      "Asia/Tokyo",
                layout:      LayoutFormat(),
            },
            wantToTimeStr: "2000-10-10 11:00:00",
            wantErr:       false,
        },
    }
    for _, tt := range tests {
        t.Run(tt.name, func(t *testing.T) {
            gotToTimeStr, err := ChangeZoneTime(tt.args.fromTimeStr, tt.args.fromZone, tt.args.toZone, tt.args.layout)
            if (err != nil) != tt.wantErr {
                t.Errorf("ChangeZoneTime() error = %v, wantErr %v", err, tt.wantErr)
                return
            }
            if gotToTimeStr != tt.wantToTimeStr {
                t.Errorf("ChangeZoneTime() gotToTimeStr = %v, want %v", gotToTimeStr, tt.wantToTimeStr)
            }
        })
    }
}
