package time

import "time"

// ChangeZoneTime 根据时区获取用户的当地时间
// fromTimeStr 原来的时间，如 "2000-10-10 10:00:00"
// fromZone 原来的时区，如 "Asia/Shanghai"
// toZone 目标时区，如 "UTC"
// layout 时间输入/输出格式,如 "2006-01-02 15:04:05"
func ChangeZoneTime(fromTimeStr, fromZone, toZone, layout string) (toTimeStr string, err error) {
    fromLocation, err := time.LoadLocation(fromZone)
    if err != nil {
        return
    }
    fromTime, err := time.ParseInLocation(layout, fromTimeStr, fromLocation)
    if err != nil {
        return
    }
    toLocation, err := time.LoadLocation(toZone)
    if err != nil {
        return
    }
    toTimeStr = time.Unix(fromTime.Unix(), 0).In(toLocation).Format(layout)
    return
}

// LayoutFormat 常规的用户时间格式
func LayoutFormat() string {
    return "2006-01-02 15:04:05"
}
