package json

import "testing"

func TestAscJson(t *testing.T) {
	type args struct {
		inputJSON string
	}
	tests := []struct {
		name           string
		args           args
		wantOutputJSON string
		wantErr        bool
	}{
		{
			name: "case 1",
			args: args{
				inputJSON: `{"key2":"value2","key1":"value1","key3":"value3"}`,
			},
			wantOutputJSON: `{"key1":"value1","key2":"value2","key3":"value3"}`,
			wantErr:        false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			gotOutputJSON, err := AscJson(tt.args.inputJSON)
			if (err != nil) != tt.wantErr {
				t.Errorf("AscJson() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if gotOutputJSON != tt.wantOutputJSON {
				t.Errorf("AscJson() gotOutputJSON = %v, want %v", gotOutputJSON, tt.wantOutputJSON)
			}
		})
	}
}

func TestStringMap2Json(t *testing.T) {
	type args struct {
		m    map[string]any
		keys []string
	}
	tests := []struct {
		name    string
		args    args
		want    string
		wantErr bool
	}{
		// TODO: Add test cases.
		{
			name: "简单字符串",
			args: args{
				m: map[string]any{
					"d": "valued",
					"b": "valueB",
					"a": "valueA",
					"c": "valueC",
				},
				keys: []string{"c", "b", "d", "a"},
			},
			want: `{
    "c": "valueC",
    "b": "valueB",
    "d": "valued",
    "a": "valueA"
}`,
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := StringMap2Json(tt.args.m, tt.args.keys)
			if (err != nil) != tt.wantErr {
				t.Errorf("StringMap2Json() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if got != tt.want {
				t.Errorf("StringMap2Json() got = %v, want %v", got, tt.want)
			}
		})
	}
}
