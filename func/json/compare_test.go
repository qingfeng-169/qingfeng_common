package json

import (
	"reflect"
	"testing"
)

func TestDiffKey(t *testing.T) {
	type args struct {
		input1 string
		input2 string
	}
	tests := []struct {
		name        string
		args        args
		wantOutput1 []string
		wantOutput2 []string
		wantErr     bool
	}{
		{
			name: "case1",
			args: args{
				input1: `{"key1":"value1","key2":"value2","key3":"value3"}`,
				input2: `{"key1":"value1","key3":"value3","key4":"value4"}`,
			},
			wantOutput1: []string{"key2"},
			wantOutput2: []string{"key4"},
			wantErr:     false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			gotOutput1, gotOutput2, err := DiffKey(tt.args.input1, tt.args.input2)
			if (err != nil) != tt.wantErr {
				t.Errorf("DiffKey() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(gotOutput1, tt.wantOutput1) {
				t.Errorf("DiffKey() gotOutput1 = %v, want %v", gotOutput1, tt.wantOutput1)
			}
			if !reflect.DeepEqual(gotOutput2, tt.wantOutput2) {
				t.Errorf("DiffKey() gotOutput2 = %v, want %v", gotOutput2, tt.wantOutput2)
			}
		})
	}
}
