package json

import (
	"encoding/json"
	"log"
)

// DiffKey 输入2个json字符串，
// 输出在第1个字符串中存在，第2个字符串中不存在的key;
// 输出在第2个字符串中存在，第1个字符串中不存在的key;
func DiffKey(input1 string, input2 string) (output1 []string, output2 []string, err error) {
	// 解析输入JSON到map
	var map1 map[string]any
	var map2 map[string]any
	err = json.Unmarshal([]byte(input1), &map1)
	if err != nil {
		log.Fatalf("%s 不能转为结构体\n", input1)
		return
	}
	err = json.Unmarshal([]byte(input2), &map2)
	if err != nil {
		log.Fatalf("%s 不能转为结构体\n", input2)
		return
	}

	for k := range map1 {
		if _, ok := map2[k]; !ok {
			output1 = append(output1, k)
		}
	}
	for k := range map2 {
		if _, ok := map1[k]; !ok {
			output2 = append(output2, k)
		}
	}
	return
}

// OnlyInput1 输入2个json字符串，
// 输出仅在第1个字符串中存在的键值对
func OnlyInput1(input1 string, input2 string) (output map[string]any, err error) {
	// 解析输入JSON到map
	var map1 map[string]any
	var map2 map[string]any
	err = json.Unmarshal([]byte(input1), &map1)
	if err != nil {
		log.Fatalf("%s 不能转为结构体\n", input1)
		return
	}
	err = json.Unmarshal([]byte(input2), &map2)
	if err != nil {
		log.Fatalf("%s 不能转为结构体\n", input2)
		return
	}

	output = make(map[string]any)
	for k, v := range map1 {
		if _, ok := map2[k]; !ok {
			output[k] = v
		}
	}
	return
}
