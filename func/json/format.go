package json

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"log"
	"sort"
)

// AscJson 把输入的json字符串，按照key升序，返回新的json字符串
// inputJSON 输入的inputJSON，如{"key2":"value2","key1":"value1","key3":"value3"}
// 输出格式，如{"key1":"value1","key2":"value2","key3":"value3"}
func AscJson(inputJSON string) (outputJSON string, err error) {
	// 解析输入JSON到map
	var data map[string]any
	err = json.Unmarshal([]byte(inputJSON), &data)
	if err != nil {
		log.Fatalf("%s 不能转为结构体\n", inputJSON)
		return
	}
	dataLen := len(data)
	// 将map的键提取到切片中
	keys := make([]string, 0, dataLen)
	for k := range data {
		keys = append(keys, k)
	}

	// 对键切片进行排序
	sort.Strings(keys)

	// 根据排序后的键切片，重新构造一个新的有序map
	sortedData := make(map[string]any, dataLen)
	for _, k := range keys {
		sortedData[k] = data[k]
	}

	// 将有序map编码回JSON
	jsonByte, err := json.Marshal(sortedData)
	//jsonByte, err := json.MarshalIndent(sortedData, "", "  ")
	if err != nil {
		log.Fatalf("%v 编码JSON失败,原因：%v\n", sortedData, err)
		return
	}
	return string(jsonByte), nil
}

// StringMap2Json 把map按照keys的顺序返回json字符串
// 因为map是无序的，所以需要按照keys的顺序返回json字符串
// m 输入的map
// keys 按照keys的顺序返回json字符串
func StringMap2Json(m map[string]any, keys []string) (string, error) {
	// 创建一个缓冲区来存储有序的JSON对象
	var buffer bytes.Buffer
	buffer.WriteString("{\n")
	for i, k := range keys {
		if k == "" {
			return "", errors.New("key is empty")
		}
		v, ok := m[k]
		if !ok {
			return "", errors.New(fmt.Sprintf("%s在map中没有对应的值", k))
		}
		kvPair, _ := json.MarshalIndent(map[string]any{k: v}, "", "    ")
		// 去掉最外层的大括号
		kvPair = kvPair[2 : len(kvPair)-2]
		buffer.Write(kvPair)
		if i < len(keys)-1 {
			buffer.WriteString(",")
		}
		buffer.WriteString("\n")
	}
	buffer.WriteString("}")

	return buffer.String(), nil
}
